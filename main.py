from pytube import YouTube
import os
from pathlib import Path
from moviepy.editor import *
import ntpath
import re
import sys

VIDEO_MP4_360P_ITAG_VALUE = 18

ROOT_PATH = Path(__file__).parent


def print_done(_stream, file_path):
    print(f"Download completed - transforming to mp3 ...")
    video = VideoFileClip(file_path)
    video_filename = ntpath.basename(file_path)
    audio_filename = re.sub(r'\s+', "_", video_filename).lower().replace("mp4", "mp3")

    out_filepath = os.path.join(ROOT_PATH, "audio_files", audio_filename)
    video.audio.write_audiofile(out_filepath)
    os.remove(file_path)


def print_progress(_stream, _chunk, bytes_remaining):
    print(f"Remaining bytes: {bytes_remaining}")


def download_mp3_from_yt_url(url: str):
    yt = YouTube(
        url,
        on_progress_callback=print_progress,
        on_complete_callback=print_done,
    )
    print(f"Downloading {yt.title} ...")
    filtered_streams = yt.streams.filter(
        file_extension='mp4'
    )
    stream = filtered_streams.get_by_itag(VIDEO_MP4_360P_ITAG_VALUE)
    if stream:
        output_path = os.path.join(ROOT_PATH, "downloads")
        stream.download(output_path=output_path)
    else:
        print("Could not download the video")


def run():
    # e.g. url = 'https://youtu.be/2lAe1cqCOXo'
    url = sys.argv[1]
    if not url:
        exit(1)

    download_mp3_from_yt_url(url)


if __name__ == '__main__':
    run()
